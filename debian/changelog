xnee (3.19-9.3) UNRELEASED; urgency=medium

  * Non-maintainer upload.
  * Mark xnee-doc as M-A: foreign.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 16 Feb 2025 13:53:41 +0100

xnee (3.19-9.2) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Non-maintainer upload.
  * Fix building twice in a row. (Closes: #1046303)
  * Switch B-D from transitional pkg-config to pkgconf.
  * Declare Rules-Requires-Root: no.
  * Update Lintian overrides.

  [ Nick Rosbrook ]
  * d/p/fix-implicit-declarations.patch: fix FTBFS due to implicit
    declarations (LP: #2059148) (Closes: #1066278)

 -- Andreas Beckmann <anbe@debian.org>  Fri, 03 May 2024 22:04:57 +0200

xnee (3.19-9.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1063283

 -- Steve Langasek <vorlon@debian.org>  Thu, 29 Feb 2024 07:46:12 +0000

xnee (3.19-9) unstable; urgency=medium

  * d/control: remove dependency on install-info (Closes: #1013865)
  * d/patches: update patch for IM policy for parallel builds

 -- Vincent Bernat <bernat@debian.org>  Sun, 26 Jun 2022 08:35:51 +0200

xnee (3.19-8) unstable; urgency=medium

  * d/rules: make xnee reproducible, thanks to Vagrant Cascadian.
    Closes: #995960.

 -- Vincent Bernat <bernat@debian.org>  Sun, 10 Oct 2021 21:06:25 +0200

xnee (3.19-7) unstable; urgency=medium

  * d/patches: add patch by Vagrant Cascadian to improve reproducibility.
    Closes: #995747.

 -- Vincent Bernat <bernat@debian.org>  Thu, 07 Oct 2021 21:55:33 +0200

xnee (3.19-6) unstable; urgency=medium

  * d/control: fix homepage location. Closes: #986991.
  * d/watch: use HTTP instead of FTP, move upstream GPG key.
  * d/patches: add patch by Dennis Filder to fix FTBFS. Closes: #991068.

 -- Vincent Bernat <bernat@debian.org>  Fri, 16 Jul 2021 20:12:40 +0200

xnee (3.19-5) unstable; urgency=medium

  [ Peter Pentchev ]
  * Add the enum-decl patch to build with GCC 10. Closes: #957989.

 -- Vincent Bernat <bernat@debian.org>  Tue, 04 Aug 2020 09:36:47 +0200

xnee (3.19-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No-change source-only upload to allow testing migration.

 -- Boyuan Yang <byang@debian.org>  Fri, 18 Oct 2019 11:15:17 -0400

xnee (3.19-4) unstable; urgency=medium

  * d/control: build-depends on texlive-plain-generic. Closes: #941553.

 -- Vincent Bernat <bernat@debian.org>  Wed, 02 Oct 2019 05:53:09 +0200

xnee (3.19-3) unstable; urgency=medium

  * d/control: update Vcs-* fields.
  * d/control: drop gnee. Closes: #923918.

 -- Vincent Bernat <bernat@debian.org>  Wed, 03 Apr 2019 07:22:54 +0200

xnee (3.19-2) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Add debian/patches/upstream_libgnomui-only-for-applets.patch
    - upstream commit/revision 1.134->1.135 to not require
      libgnomeui unless building applets (which is disabled here).
  * Drop libgnomeui-dev build-dependency. (Closes: #885738)
  * Add missing build-dependencies on pkg-config and libgtk2.0-dev
    - previously implicitly pulled in via libgnomeui-dev

  [ Vincent Bernat ]
  * Use debhelper 9.
  * Move to dbgsym for debug package.

 -- Vincent Bernat <bernat@debian.org>  Tue, 02 Jan 2018 10:00:22 +0100

xnee (3.19-1) unstable; urgency=medium

  * New upstream version.
  * Add dependency on texlive-generic-recommended to fix FTBFS.
  * Run autoreconf on build. Closes: #744682.

 -- Vincent Bernat <bernat@debian.org>  Fri, 23 May 2014 01:06:56 +0200

xnee (3.18-1) unstable; urgency=low

  * New upstream version.
     + Fix crash in gnee on startup. Closes: #737543.
     + Fix crash when missing arguments. Closes: #715746.
  * Update debian/watch to check GPG signatures.
  * Fix location of xnee.info. Closes: #719446.
  * Bump Standards-Version.

 -- Vincent Bernat <bernat@debian.org>  Mon, 17 Mar 2014 21:18:53 +0100

xnee (3.13-1) unstable; urgency=low

  * New upstream version.
  * Bump Standards-Version.

 -- Vincent Bernat <bernat@debian.org>  Sun, 13 May 2012 09:31:43 +0200

xnee (3.11-1) unstable; urgency=low

  * New upstream version.
     + Fix recording in X.org > 1.11. Closes: #643425.

 -- Vincent Bernat <bernat@debian.org>  Sat, 17 Dec 2011 17:18:23 +0100

xnee (3.10-1) unstable; urgency=low

  * New upstream version.
  * Drop pnee since it has not been ported to the new libpanel-applet
    interface. Closes: #638111
  * Fix manual pages. Closes: #643340, #643342, #643346.
  * Bump Standards-Version.

 -- Vincent Bernat <bernat@debian.org>  Sat, 15 Oct 2011 13:01:13 +0200

xnee (3.09-1) unstable; urgency=low

  * New upstream release. Closes: #613237.
     + Now compile fine with binutils-gold. Closes: #556714.
     + Enable documentation compilation.
  * Bump Standards-Version.
  * Fix a typo in description of libxnee0. Closes: #592778.
  * Fix xnee.info. Closes: #605666, #605667.

 -- Vincent Bernat <bernat@debian.org>  Sat, 02 Apr 2011 14:06:51 +0200

xnee (3.06-1) unstable; urgency=low

  * New upstream version.
     + remove no-doc-clean.patch
     + remove correct-doc-path.patch
     + remove fix-compilation-error.patch
     + add docdir-fix.patch to install documentation in the correct location.
  * Update Standards-Version to 3.8.4. No changes required.
  * Revert the last change about ldconfig since this is already done by
    dh_makeshlibs.
  * Re-enable pnee which seems to be fixed. Closes: #509638.
  * Drop postinst and prerm for xnee-doc: all work is now done by triggers.
  * xnee-doc depends on "dpkg (>= 1.15.4) | install-info", as recommended
    per policy.
  * Correction section of libxnee-dbg to "debug".
  * Don't ship PDF anymore: we already have info, HTML and text.
  * Acknowledge previous NMU (Closes: #536934). The problem is now fixed
    upstream.

 -- Vincent Bernat <bernat@debian.org>  Sat, 12 Jun 2010 13:23:01 +0200

xnee (3.02-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * configure.in: lookup pkg-config data also for libgnomeui-2.0 (now
    needed for gnome.h); rebuild ./configure accordingly. Fix FTBFS.
    (Closes: #536934)
  * debian/control: add build-dep on libgnomeui-dev

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 05 Sep 2009 22:58:42 +0200

xnee (3.02-2) unstable; urgency=low

  * Call ldconfig in postinst and postrm for libxnee0.
  * Small fix to debian/copyright

 -- Vincent Bernat <bernat@debian.org>  Sun, 03 Aug 2008 10:27:23 +0200

xnee (3.02-1) unstable; urgency=low

  * New upstream version (Closes: #349668)
  * In debian/control:
     + bump Standards-Version to 3.8.0
     + add Homepage field
     + add Vcs-* fields
     + depends on debhelper >= 5
     + adjust build-dependencies to be able to build the new packages
     + create new packages: cnee, gnee, pnee, libxnee and xnee-doc
     + update long description (Closes: #349309)
     + adopt package, thanks to Ross Burton for his work
     + add Barry deFreese as uploader
  * Fix xnee.info (Closes: #349660)
  * Ship examples with cnee (Closes: #315457)
  * Remove debian/TODO
  * Add a patch to avoid to cleanup documentation
  * Add a patch to fix documentation installation
  * Update debian/copyright since Xnee is now GPLv3 licensed
  * We don't ship pnee since it is segfaulting

 -- Vincent Bernat <bernat@debian.org>  Thu, 24 Jul 2008 15:01:48 +0200

xnee (1.08-3) unstable; urgency=low

  * debian/control: Fix override/control differences.
  * Add debian/watch

 -- Ross Burton <ross@debian.org>  Wed, 30 Mar 2005 11:07:34 +0100

xnee (1.08-2) unstable; urgency=low

  * debian/control: build-depend on libxt-dev (closes: #300396)
  * debian/copyright: fix upstream URL

 -- Ross Burton <ross@debian.org>  Sun, 27 Mar 2005 13:48:23 +0100

xnee (1.08-1) unstable; urgency=low

  * Initial packaging.

 -- Ross Burton <ross@debian.org>  Wed, 17 Nov 2004 15:40:11 +0000
